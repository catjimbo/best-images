# BestImages

## Описание

Проект - социальная сеть, в которой пользователи могут делиться фотографиями, реагировать на них и передавать сообщения друг другу.

### Реализация

Серверная часть проекта создана на **Python** фреймворке **Django**. База данных - MySQL. Клиентская часть ручной HTML + CSS фреймворк Bulma + AJAX-script для реализации чата + библотека AOS для анимаций

**Post таблица**

| id | image | content | date_post | author_id |
| ------ | ------ | ------ | ------ | ------ |
| BigInt(Primary Key) | Varchar(path to image) | LongText(Text for Post) | DateTime | BigInt(Foreing Key) |


**Room таблица для чата**

| id | user_one_id | user_second_id |
| ------ | ------ | ------ |
| BigInt(Primary Key) | Int(Foreing Key for first user) | Int(Foreing Key for second user) |

**Message таблица для комнаты чата(сообщение отдельного чата)**

| id | value | date | username | room_id | user_id |
| ------ | ------ | ------ | ------ | ------ | ------ |
| BigInt(Primary Key) | LongText(Message Text) | DateTime(Date of Message) | Varchar | BigInt(Foreing Key for room) | BigInt(Foreing Key for user) |
