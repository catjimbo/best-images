from django.shortcuts import render
from .models import Room, Message
from django.http import HttpResponse, JsonResponse


def room(request, pk):
    username = request.user
    room_details = Room.objects.get(pk=pk)
    if room_details.user_one == username or room_details.user_second == username:
        return render(request, 'chat/room.html', {
            'username': username.username,
            'pk': pk,
            'room_details': room_details
        })


def send(request):
    message = request.POST['message']
    room_id = request.POST['room_id']

    room = Room.objects.filter(pk=room_id).first()

    new_message = Message.objects.create(value=message, user=request.user, room=room, username=request.user.username)
    new_message.save()
    return HttpResponse('Message sent successfully')


def getMessages(request, pk):
    room_details = Room.objects.get(pk=pk)
    messages = Message.objects.filter(room=room_details)
    images = []
    time = []
    for message in messages:
        date = message.date.strftime("%H:%M:%S %d-%m-%Y")
        time.append(date)
        images.append(message.user.profile.image.url)
    return JsonResponse({"messages": list(messages.values()), "images": images, "time": time})
