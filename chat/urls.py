from django.urls import path
from . import views

urlpatterns = [
    path('room/<int:pk>/', views.room, name='room'),
    path('send', views.send, name='send'),
    path('getMessages/<int:pk>/', views.getMessages, name='getMessages'),
]
