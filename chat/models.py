from django.db import models
from django.urls import reverse

from Post.models import Post
from django.contrib.auth.models import User
from django.utils import timezone


class Room(models.Model):
    user_one = models.ForeignKey(User, on_delete=models.CASCADE, related_name='author')
    user_second = models.ForeignKey(User, on_delete=models.CASCADE, related_name='customer')

    def get_absolute_url(self):
        return reverse('room', kwargs={'pk': self.pk})

    def __str__(self):
        return self.user_one.username + self.user_second.username


class Message(models.Model):
    value = models.TextField(max_length=1000)
    date = models.DateTimeField(default=timezone.now, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    username = models.CharField(max_length=256)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)

    def __str__(self):
        return self.date.strftime('%Y-%m-%d %H:%M') + self.user.username