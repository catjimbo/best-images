from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.db import models
from django.urls import path, include
from Users import views as user_views
from django.conf import settings
from django.conf.urls.static import static
from Users.forms import MyAuthenticationForm


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('Post.urls')),
    path('', include('chat.urls')),
    path('register/', user_views.register, name='register'),
    path('login/', auth_views.LoginView.as_view(template_name='Users/login.html', authentication_form=MyAuthenticationForm), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='Users/logout.html'), name='logout'),
    path('password-reset/', auth_views.PasswordResetView.as_view(template_name='Users/password_reset.html'), name='password_reset'),
    path('password-reset/done/', auth_views.PasswordResetDoneView.as_view(template_name='Users/password_reset_done.html'), name='password_reset_done'),
    path('password-reset/confirm/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name='USERS/password_reset_confirm.html'), name='password_reset_confirm'),
    path('password-reset-complete/', auth_views.PasswordResetCompleteView.as_view(template_name='Users/password_reset_complete.html'), name='password_reset_complete'),
    path('profile/<str:user>', user_views.profile, name='profile'),
    path('edit/', user_views.profile_edit, name='edit-profile'),
    path('activate/<uidb64>/<token>', user_views.activate, name='activate'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
